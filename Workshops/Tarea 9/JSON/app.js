window.onload = function() {

    const localStorageName = "data";

    loadUsers();

    document.querySelector("#btn-add").addEventListener("click", () => {

        let name = document.getElementById("name");
        let lastName = document.getElementById("lastName");
        let phone = document.getElementById("phone");

        let user = {
            name: name.value,
            lastName: lastName.value,
            phone: phone.value
        };

        name.value = "";
        lastName.value = "";
        phone.value = "";

        registerUser(user);

    });

    function registerUser(user) {
        let users = [];
        let userInLocalStorage = localStorage.getItem(localStorageName);
        if (userInLocalStorage !== null) {
            users = JSON.parse(userInLocalStorage);
        }
        users.push(user);
        localStorage.setItem(localStorageName, JSON.stringify(users));
        loadUsers();
    }

    function loadUsers() {
        let users = [];
        let userInLocalStorage = localStorage.getItem(localStorageName);
        let tbody = document.querySelector("#grid tbody");
        if (userInLocalStorage !== null) {
            users = JSON.parse(userInLocalStorage);
        }

        tbody.innerHTML = "";

        users.forEach(function(x, i) {
            let tr = document.createElement("tr");
            let tName = document.createElement("td");
            let tLastName = document.createElement("td");
            let tPhone = document.createElement("td");
            let tButton = document.createElement("td");
            let btnDelete = document.createElement("button");

            tName.appendChild(document.createTextNode(x.name));
            tLastName.appendChild(document.createTextNode(x.lastName));
            tPhone.appendChild(document.createTextNode(x.phone));

            btnDelete.textContent = "Delete";
            btnDelete.className = "btn btn-xs btn-danger";
            btnDelete.addEventListener("click", () => {
                deleteUser(i);
            });
            tButton.appendChild(btnDelete);

            tr.appendChild(tName);
            tr.appendChild(tLastName);
            tr.appendChild(tPhone);
            tr.appendChild(tButton);

            tbody.appendChild(tr);

        });

    }

    function deleteUser(i) {
        users = JSON.parse(localStorage.getItem(localStorageName));
        users.splice(i, 1);
        localStorage.setItem(localStorageName, JSON.stringify(users));
        loadUsers();
    }

}