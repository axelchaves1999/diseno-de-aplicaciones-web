$(document).ready(() => {

    const userLogedInLocalStorageName = "loged";
    const productsInLocalStorageName = "products";
    const viewedProductsName = "viewedProducts";
    const localStorageUsersName = "user";
    const detailProductName = "detailProduct"
    const detailPageName = "DetalleProducto.html";

    const initationPageName = "Inicio.html";
    const dashboardPageName = "Dashboard.html";
    const loginPageName = "Login.html";

    loadStatus();


    $(window).resize(() => {
        loadScreenSize();
    });

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }




    $(".log").click(() => {
        let user = getUserLoged();
        if (user !== null && user !== undefined) {
            redirection(dashboardPageName);
        } else {
            redirection(loginPageName);
        }
    })


    function loadStatus() {
        let user = getUserLoged();
        let start = document.getElementsByClassName("log");

        if (user !== null && user !== undefined) {
            start[0].innerHTML = "Dashboard";
            start[1].innerHTML = "Dashboard";
        }
        loadMyProducts();

    }

    function loadMyProducts() {
        let products = [];
        let productsInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (productsInLocalStorageName !== null) {
            products = JSON.parse(productsInLocalStorage);
            loadProducts(products);

        }

    }


    function loadProducts(products) {
        if (products !== null) {

            let row = document.querySelector("main .row");
            row.innerHTML = "";

            for (let i = 0; i < products.length; i++) {

                let mainDiv = document.createElement("div");
                mainDiv.className = "card border-dark mb-3 ml-2";
                mainDiv.setAttribute("style", "max-width: 20rem;  ");

                let hearderDiv = document.createElement("div");
                hearderDiv.className = "card text-center";
                mainDiv.appendChild(hearderDiv);

                let cardhearderDiv = document.createElement("div");
                cardhearderDiv.className = "card-header";
                hearderDiv.appendChild(cardhearderDiv);

                let list = document.createElement("ul");
                list.className = "nav nav-pills card-header-pills";
                cardhearderDiv.appendChild(list);

                let firstElementLi = document.createElement("li");
                firstElementLi.className = "nav-item pr-2 pb-2";
                list.appendChild(firstElementLi);

                let firstTagA = document.createElement("button");
                firstTagA.className = "btn btn-info text-white ";
                firstTagA.appendChild(document.createTextNode(products[i].name));
                firstTagA.addEventListener("click", () => {
                    detailProduct(products[i]);
                });
                firstElementLi.appendChild(firstTagA);


                let secondElementLi = document.createElement("li");
                secondElementLi.className = "nav-item ";
                list.appendChild(secondElementLi);

                let secondTagA = document.createElement("a");
                secondTagA.className = "nav-link bg-dark text-white ";
                secondTagA.appendChild(document.createTextNode(getUser(products[i]).name));
                secondElementLi.appendChild(secondTagA);

                let cardDiv = document.createElement("div");
                cardDiv.className = "card-body";
                cardhearderDiv.appendChild(cardDiv);

                let img = document.createElement("img");
                img.className = "img-fluid ";
                img.setAttribute("style", "width: 200px; height: 200px;");
                img.setAttribute("src", products[i].link);
                cardDiv.appendChild(img);


                row.appendChild(mainDiv);
            }
        }
    }

    function detailProduct(product) {
        localStorage.setItem(detailProductName, JSON.stringify(product));
        viewedProduct(product);
        redirection(detailPageName);

    }


    function getUserLoged() {
        return JSON.parse(localStorage.getItem(userLogedInLocalStorageName));
    }

    function viewedProduct(product) {
        let products = [];
        let viewedProductsInLocalStorage = localStorage.getItem(viewedProductsName);
        if (viewedProductsInLocalStorage !== null) {
            products = JSON.parse(viewedProductsInLocalStorage);
        }
        product.idUserView = getUserLoged().id;
        products.push(product);
        localStorage.setItem(viewedProductsName, JSON.stringify(products));

    }

    function getUser(product) {
        let users = [];
        let usersInLocalStorage = localStorage.getItem(localStorageUsersName);
        if (usersInLocalStorage !== null) {
            users = JSON.parse(usersInLocalStorage);
        }
        for (let i = 0; i < users.length; i++) {
            if (users[i].id === product.idUser) {
                return users[i];
            }

        }
    }

    function getUserLoged() {
        return JSON.parse(localStorage.getItem(userLogedInLocalStorageName));
    }

    function redirection(page) {
        location.replace(page);
    }


});