$(document).ready(function() {

    const productsInLocalStorageName = "products";
    const userLogedInLocalStorageName = "loged";
    const productChangeName = "product";
    const redirectPageName = "Dashboard.html";
    const serialProductsName = "serialProductos";

    loadStatus();

    $("#cancel-button").click(() => {
        localStorage.removeItem(productChangeName);
        redirection();
    })

    $(window).resize(() => {
        loadScreenSize();
    });

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }

    $("#add-button").click(() => {

        let name = document.getElementById("name");
        let description = document.getElementById("description");
        let link = document.getElementById("link");
        let looking = document.getElementById("looking");

        let productToChange = getStatus();
        if (productToChange !== undefined) {
            productToChange.name = name.value;
            productToChange.description = description.value;
            productToChange.link = link.value;
            productToChange.looking = looking.value;
            editProduct(productToChange);

        } else {


            let product = {
                id: 0,
                idUser: 0,
                name: name.value,
                description: description.value,
                link: link.value,
                looking: looking.value,
                toString: function() {
                    return this.name;
                }
            };


            registerProduct(product)
        }

    });

    function registerProduct(product) {
        let products = [];
        let productsInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (productsInLocalStorage !== null) {
            products = JSON.parse(productsInLocalStorage);
        }

        product.id = getSerial();
        product.idUser = getUserLoged().id;
        products.push(product);

        localStorage.setItem(productsInLocalStorageName, JSON.stringify(products));
        redirection();

    }

    function editProduct(product) {
        let products = [];
        let productInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (productInLocalStorage !== null) {
            products = JSON.parse(productInLocalStorage);
            for (let i = 0; i < products.length; i++) {
                if (products[i].id === product.id) {
                    products[i].name = product.name;
                    products[i].description = product.description;
                    products[i].link = product.link;
                    products[i].looking = product.looking;
                }

            }
        }
        localStorage.setItem(productsInLocalStorageName, JSON.stringify(products));
        localStorage.removeItem(productChangeName);
        redirection();

    }


    function getSerial() {
        let serial = 0;
        let serialInlocalStorage = localStorage.getItem(serialProductsName);
        if (serialInlocalStorage !== null) {
            serial = parseInt(JSON.parse(serialInlocalStorage));
        }
        serial++;
        localStorage.setItem(serialProductsName, JSON.stringify(serial));
        return serial;
    }

    function loadStatus() {
        let product = getStatus();
        if (product !== undefined) {
            document.getElementById("name").value = product.name;
            document.getElementById("description").value = product.description;
            document.getElementById("link").value = product.link;
            document.getElementById("looking").value = product.looking;
        }

    }

    function getStatus() {
        let productInLocalStorage = localStorage.getItem(productChangeName);
        if (productInLocalStorage !== null) {
            return JSON.parse(productInLocalStorage);
        }
    }

    function getUserLoged() {
        return JSON.parse(localStorage.getItem(userLogedInLocalStorageName));
    }

    function redirection() {
        location.replace(redirectPageName);
    }


});