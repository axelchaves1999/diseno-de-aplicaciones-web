$(document).ready(function() {

    const localStorageUsersName = "user";
    const userLogedInLocalStorageName = "loged";
    const redirectPageName = "Dashboard.html";


    $(window).resize(() => {
        loadScreenSize();
    });

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }

    $("#register").click(function(event) {
        event.preventDefault();

        if (validate() === true) {

            let name = document.getElementById("name");
            let lastName = document.getElementById("lastName");
            let address = document.getElementById("address");
            let address2 = document.getElementById("address2");
            let country = document.getElementById("country");
            let city = document.getElementById("city");
            let email = document.getElementById("email");
            let password = document.getElementById("password");


            let user = {
                id: 0,
                name: name.value,
                lastName: lastName.value,
                address: address.value,
                address2: address2.value,
                country: country.value,
                city: city.value,
                email: email.value,
                password: password.value
            };
            userRegister(user);
        }


    });

    function validate() {
        let inputs = document.querySelectorAll("form input[type=text],input[type=email],input[type=password]");
        for (let i = 0; i < inputs.length; i++) {
            if (inputs[i].value.length <= 0) {
                return false;
            }
        }
        return true;

    }


    function userRegister(user) {
        let users = [];
        let usersInLocalStorage = localStorage.getItem(localStorageUsersName);
        if (usersInLocalStorage !== null) {
            users = JSON.parse(usersInLocalStorage);
        }

        user.id = users.length + 1;
        users.push(user);
        localStorage.setItem(localStorageUsersName, JSON.stringify(users));
        redirection(user);

    }

    function redirection(user) {
        localStorage.setItem(userLogedInLocalStorageName, JSON.stringify(user));
        location.replace(redirectPageName);
    }



});