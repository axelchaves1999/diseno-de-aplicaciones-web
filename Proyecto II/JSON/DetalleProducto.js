$(document).ready(() => {

    const detailProductName = "detailProduct"
    const localStorageUsersName = "user";
    const userLogedInLocalStorageName = "loged";
    const dashboardPageName = "Dashboard.html";
    const loginPageName = "Login.html";


    loadStatus();

    $(".log").click(() => {
        let user = getUserLoged();
        if (user !== null && user !== undefined) {
            redirectionPage(dashboardPageName);
        } else {
            redirectionPage(loginPageName);
        }
    })

    $(window).resize(() => {
        loadScreenSize();
    });

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }

    function loadStatus() {
        let user = getUserLoged();
        let start = document.getElementsByClassName("log");

        if (user !== null && user !== undefined) {
            start[0].innerHTML = "Dashboard";
            start[1].innerHTML = "Dashboard";
        }
        loadDetailProduct();
    }



    function loadDetailProduct() {
        let product = JSON.parse(localStorage.getItem(detailProductName));
        if (product !== undefined) {

            let containerImage = document.querySelector("#container-image");
            containerImage.innerHTML = "";

            let image = document.createElement("img");
            image.className = "img-responsive";
            image.setAttribute("style", "width: 100%; height: 495px;");
            image.setAttribute("src", product.link);
            containerImage.appendChild(image);


            let containerTitule = document.querySelector("#container-titule");
            containerTitule.innerHTML = "";

            let hTitule = document.createElement("h1");
            hTitule.appendChild(document.createTextNode(product.name));
            containerTitule.appendChild(hTitule);

            let pTitule = document.createElement("p");
            pTitule.appendChild(document.createTextNode("Ofrecido por:"));
            let aTitule = document.createElement("a");
            aTitule.appendChild(document.createTextNode(getUser(product).name));
            pTitule.appendChild(aTitule);
            containerTitule.appendChild(pTitule);

            let containerDescription = document.querySelector("#container-description");
            containerDescription.innerHTML = "";

            let hDescription = document.createElement("h5");
            hDescription.appendChild(document.createTextNode("Descripción"));
            containerDescription.appendChild(hDescription);

            let pDescription = document.createElement("p");
            pDescription.appendChild(document.createTextNode(product.description));
            containerDescription.appendChild(pDescription);



            let containerLooking = document.querySelector("#container-looking");
            containerLooking.innerHTML = "";

            let hLooking = document.createElement("h5");
            hLooking.appendChild(document.createTextNode("Busco"));
            containerDescription.appendChild(hLooking);

            let pLooking = document.createElement("p");
            pLooking.appendChild(document.createTextNode(product.looking));
            containerDescription.appendChild(pLooking);




        }
    }

    function redirectionPage(page) {
        location.replace(page);
    }

    function getUserLoged() {
        return JSON.parse(localStorage.getItem(userLogedInLocalStorageName));
    }

    function getUser(product) {
        let users = [];
        let usersInLocalStorage = localStorage.getItem(localStorageUsersName);
        if (usersInLocalStorage !== null) {
            users = JSON.parse(usersInLocalStorage);
        }
        for (let i = 0; i < users.length; i++) {
            if (users[i].id === product.idUser) {
                return users[i];
            }

        }
    }

});