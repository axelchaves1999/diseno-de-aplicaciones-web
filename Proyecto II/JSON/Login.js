$(document).ready(function() {

    const localStorageUsersName = "user";
    const userLogedInLocalStorage = "loged";
    const redirectPage = "Dashboard.html";




    $("#email").keypress(() => {
        let email = document.getElementById("email").value;
        if (email.length >= 2) {
            let alert = document.querySelector("#alert");

            if (alert !== undefined && alert !== null) {
                alert.remove();
            }

        }

    });

    $(window).resize(() => {
        loadScreenSize();
    });

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }

    $("#ingreso").click(function(event) {
        event.preventDefault();

        let email = document.getElementById("email");
        let password = document.getElementById("password");

        if (email.value == "" || password.value == "") return;

        let user = {
            email: email.value,
            password: password.value
        };

        validation(user);
    });

    function validation(user) {

        let getUser = login(user);
        if (getUser !== null) {
            redirection(getUser);
        } else {
            creatAlet();
            clean();
        }

    }

    function clean() {
        document.getElementById("email").value = "";
        document.getElementById("password").value = "";
    }



    function login(user) {
        let users = [];

        let usersInLocalStorage = localStorage.getItem(localStorageUsersName);
        if (usersInLocalStorage !== null) {
            users = JSON.parse(usersInLocalStorage);
        }

        for (let i = 0; i < users.length; i++) {
            if (users[i].email === user.email && users[i].password === user.password) {
                return users[i];
            }

        }
        return null;

    }


    function redirection(user) {
        localStorage.setItem(userLogedInLocalStorage, JSON.stringify(user));
        location.replace(redirectPage);
    }

    function creatAlet() {
        let form = document.querySelector("#form");
        let div = document.createElement("div");
        div.className = "alert alert-primary";
        div.setAttribute("role", "alert");
        div.setAttribute("id", "alert");
        div.appendChild(document.createTextNode("Usuario invalido"));
        form.appendChild(div);
    }

});