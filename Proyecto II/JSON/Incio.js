$(document).ready(() => {

    const viewedProductsName = "viewedProducts";
    const userLogedInLocalStorageName = "loged";
    const productsInLocalStorageName = "products";
    const detailProductName = "detailProduct"
    const detailPageName = "DetalleProducto.html";

    const dashboardPageName = "Dashboard.html";
    const loginPageName = "Login.html";


    loadStatus();

    $(function() {

        let products = [];
        let myProducts = [];
        let viewedProductsInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (viewedProductsName !== null) {
            products = JSON.parse(viewedProductsInLocalStorage);
        }
        if (products !== null) {
            for (let i = 0; i < products.length; i++) {
                let format = products[i].id + "-" + products[i].name;
                myProducts.push(format);

            }

            $("#tag").autocomplete({

                source: myProducts

            });
        }


    })

    $("#btn-search").click(() => {
        let tag = document.querySelector("#tag").value;
        if (tag === null || !tag.includes('-')) return;
        let info = tag.split("-");
        let id = info[0];
        if (!isNaN(id)) {
            searchProduct(id);
        }

    })

    $(window).resize(() => {
        loadScreenSize();
    });

    $("#log-out").click(() => {
        logOut();
    });


    $(".log").click(() => {
        let user = getUserLoged();
        if (user !== null && user !== undefined) {
            redirectionPage(dashboardPageName);
        } else {
            redirectionPage(loginPageName);
        }
    })

    function searchProduct(id) {

        let products = [];
        let viewedProductsInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (viewedProductsName !== null) {
            products = JSON.parse(viewedProductsInLocalStorage);
        }
        if (products !== null) {
            for (let i = 0; i < products.length; i++) {

                if (products[i].id == id) {

                    detailProduct(products[i]);
                }

            }
        }
    }

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }

    function loadStatus() {
        let user = getUserLoged();
        let start = document.getElementsByClassName("log");
        let logOut = document.querySelector("#modal");

        if (user !== null && user !== undefined) {
            logOut.style.display = "block";
            start[0].innerHTML = "Dashboard";
            start[1].innerHTML = "Dashboard";
        } else {
            logOut.style.display = "none";
        }
        loadData();
    }


    function loadData() {
        let products = [];
        let myProducts = [];
        let viewedProductsInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (viewedProductsName !== null) {
            products = JSON.parse(viewedProductsInLocalStorage);
        }

        if (products !== null && products.length > 1) {
            myProducts.push(products[products.length - 1]);
            myProducts.push(products[products.length - 2]);
        } else {
            myProducts.push(products);
        }
        loadMyProducts(myProducts);

    };



    function loadMyProducts(dubproducts) {
        let products = dubproducts;

        if (products !== null && products.length > 1) {

            let carousel = document.querySelector(".carousel-inner");
            carousel.innerHTML = "";
            for (let i = 0; i < products.length; i++) {


                let carouselItem = document.createElement("div");
                if (i === 0) {
                    carouselItem.className = "carousel-item active text-center ";
                } else {
                    carouselItem.className = "carousel-item text-center ";
                }



                let fisrtImg = document.createElement("img");
                fisrtImg.setAttribute("style", "width: 100%; height: 220px;");
                fisrtImg.className = "img-fluid w-25";
                fisrtImg.setAttribute("src", products[i].link);

                fisrtImg.addEventListener("click", function() {
                    detailProduct(products[i]);
                });

                carouselItem.appendChild(fisrtImg);

                let div = document.createElement("div");
                div.className = "pt-2";
                carouselItem.appendChild(div);

                carousel.appendChild(carouselItem);

            }
        }
    }

    function logOut() {
        localStorage.removeItem(userLogedInLocalStorageName);
        location.reload();
    }

    function detailProduct(product) {
        localStorage.setItem(detailProductName, JSON.stringify(product));
        redirectionPage(detailPageName);
    }

    function redirectionPage(page) {
        location.replace(page);
    }

    function getUserLoged() {
        return JSON.parse(localStorage.getItem(userLogedInLocalStorageName));
    }
});