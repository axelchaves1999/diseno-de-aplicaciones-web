window.onload = function() {

    const productsInLocalStorageName = "products";
    const userLogedInLocalStorageName = "loged";
    const productChange = "product";
    const redirectPageName = "CrearEditarProducto.html";


    loadMyProducts();

    $("#add-button").click(function() {
        redirection();
    });

    $(window).resize(() => {
        loadScreenSize();
    });

    function loadScreenSize() {
        let line = document.getElementsByClassName("line");

        if (screen.width > 1000) {

            line[0].style.display = "block"
            line[1].style.display = "block"
        } else {
            line[0].style.display = "none"
            line[1].style.display = "none"
        }
    }


    function loadMyProducts() {
        let products = [];
        let myProducts = [];
        let user = getUserLoged();
        let productsInLocalStorage = localStorage.getItem(productsInLocalStorageName);
        if (productsInLocalStorageName !== null) {
            products = JSON.parse(productsInLocalStorage);
            if (products !== null) {
                for (let i = 0; i < products.length; i++) {
                    if (products[i].idUser === user.id) {
                        myProducts.push(products[i]);
                    }
                }

                loadProducts(myProducts);
            }

        }
        loadNameUser();


    }

    function loadNameUser() {
        let name = document.getElementById("user-name");
        name.innerHTML = "";
        name.appendChild(document.createTextNode(getUserLoged().name));
    }


    function loadProducts(products) {


        if (products !== null) {

            let row = document.querySelector("main .row");
            row.innerHTML = "";



            for (let i = 0; i < products.length; i++) {

                console.log(products[i]);
                let mainDiv = document.createElement("div");
                mainDiv.className = "card border-primary mb-3 ml-5  text-center";
                mainDiv.setAttribute("style", "max-width: 20rem; ");

                let hearderDiv = document.createElement("div");
                hearderDiv.className = "card-header";
                hearderDiv.appendChild(document.createTextNode(products[i].name));
                mainDiv.appendChild(hearderDiv);

                let bodyDiv = document.createElement("div");
                bodyDiv.className = "card-body d-flex ";
                mainDiv.appendChild(bodyDiv);

                let imgDiv = document.createElement("div");
                imgDiv.className = "img";
                bodyDiv.appendChild(imgDiv);

                let img = document.createElement("img");
                img.className = "img-fluid ";
                img.setAttribute("style", "width: 200px; height: 200px;");
                img.setAttribute("src", products[i].link);
                imgDiv.appendChild(img);

                let containerDiv = document.createElement("div");
                hearderDiv.className = "container ";
                bodyDiv.appendChild(containerDiv);

                let rowDiv = document.createElement("div");
                rowDiv.className = "row pl-2";
                containerDiv.appendChild(rowDiv);

                let firstSizeDiv = document.createElement("div");
                firstSizeDiv.className = "col-sm ";
                rowDiv.appendChild(firstSizeDiv);

                let EditButton = document.createElement("button");
                EditButton.className = "btn btn-success col-12 ";
                EditButton.appendChild(document.createTextNode("Editar"));
                EditButton.addEventListener("click", function() {
                    editProduct(products[i]);
                });
                firstSizeDiv.appendChild(EditButton);


                let SecondSizeDiv = document.createElement("div");
                SecondSizeDiv.className = "col pt-3";
                rowDiv.appendChild(SecondSizeDiv);


                let DeleteButton = document.createElement("button");
                DeleteButton.className = "btn btn-danger col-12 ";
                DeleteButton.appendChild(document.createTextNode("Borrar"));
                DeleteButton.addEventListener("click", () => {
                    deleteProduct(products[i]);
                });

                SecondSizeDiv.appendChild(DeleteButton);


                row.appendChild(mainDiv);
            }




        }
    }

    function editProduct(product) {
        localStorage.setItem(productChange, JSON.stringify(product));
        redirection();
    }

    function deleteProduct(product) {
        let products = JSON.parse(localStorage.getItem(productsInLocalStorageName));
        for (let i = 0; i < products.length; i++) {
            if (products[i].id === product.id) {
                products.splice(i, 1)
            }

        }
        localStorage.setItem(productsInLocalStorageName, JSON.stringify(products));
        loadMyProducts();

    }

    function redirection() {
        location.replace(redirectPageName);
    }

    function getUserLoged() {
        return JSON.parse(localStorage.getItem(userLogedInLocalStorageName));
    }

};